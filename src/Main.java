import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;

import tienda.Tienda;

public class Main {

	public static void main(String[] args) {
		ObjectContainer base;
		// Crea/Abre Base de Datos
		base = Db4oEmbedded.openFile("tienda.db4o");
		Tienda tienda = new Tienda(base);
		tienda.login("Admin", "Admin123");

		System.out.println(tienda.listar(tienda.buscarProducto("", "")));
		System.out.println(tienda.listar(tienda.buscarUsuario("", "")));
		
		
		Formulario form = new Formulario(tienda);
		form.setVisible(true);
		
		
	}

}
