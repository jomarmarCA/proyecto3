import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.db4o.ObjectSet;

import tienda.Tienda;

@SuppressWarnings("serial")
public class Formulario extends JFrame implements ActionListener {
	Tienda tienda;

	// Componentes principales
	JPanel panel;
	JButton tiendaB;
	JButton loginPrincB;
	JButton administraionB;
	JLabel nombreUsuario;
	JButton salirB;

	// Tienda
	boolean tiendaViewBool;
	JButton carroB;
	JButton administracionB;
	// Busqueda
	JButton buscarFitroB;
	JLabel nombreProdBusquedaL;
	JLabel maxPrecioL;
	JLabel minPrecioL;
	JTextField nombreProdTF;
	JTextField maxPrecioTF;
	JTextField minPrecioTF;

	// productos
	JLabel tituloProd;
	JButton imagenB;
	JButton anade;
	JButton quita;
	JTextField cantidad;

	// Componentes Login
	JFrame loginF;
	JPanel loginP;
	JTextField userTF;
	JPasswordField passPF;
	JButton loginB;
	JButton registerB;
	JButton salirLogB;
	JLabel username;
	JLabel password;
	// Registro
	JPasswordField passCheckPF;
	JTextField nombreTF;
	JTextField emailTF;

	// Error
	JFrame errorF;
	JPanel errorP;
	JLabel errorL;

	// Componentes Administracion
	boolean administracionViewBool;
	int pestana;
	JTextArea listadoTA;
	JScrollPane containerListado;
	String cabecera;
	String data;
	JLabel[] campoL = new JLabel[6];
	JTextField[] campoTF = new JTextField[6];
	JButton guardarB;
	JButton modificarB;
	JButton borrarB;
	// Productos
	JButton adminProductosB;
	// Usuarios
	JButton adminUsersB;

	public Formulario(Tienda tienda) {
		tiendaViewBool = false;
		administracionViewBool = false;

		this.tienda = tienda;

		this.setTitle("Tienda");
		this.setSize(1280, 720);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		// Componentes pricipales
		tiendaB = new JButton("Tienda");
		tiendaB.setBounds(5, 5, 120, 30);
		tiendaB.addActionListener(this);

		administracionB = new JButton("Administración");
		administracionB.setBounds(130, 5, 120, 30);
		administracionB.addActionListener(this);

		loginPrincB = new JButton("Login");
		loginPrincB.setBounds(this.getWidth() - 100, 5, 90, 30);
		loginPrincB.addActionListener(this);

		try {
			nombreUsuario = new JLabel("Usuario: " + tienda.getUser().getNombre());
		} catch (NullPointerException e) {
			nombreUsuario = new JLabel("Usuario: Anonimo");
		}
		nombreUsuario.setBounds(this.getWidth() - 270, 5, 120, 30);

		salirB = new JButton("Salir");
		salirB.setBounds(this.getWidth() - 100, this.getHeight() - 65, 90, 30);
		salirB.addActionListener(this);

		panel = new JPanel();
		panel.setLayout(null);

		panel.add(tiendaB);
		panel.add(administracionB);
		panel.add(loginPrincB);
		panel.add(nombreUsuario);
		panel.add(salirB);

		// Tienda

		carroB = new JButton(new ImageIcon(".\\imagenes\\Carro.png"));
		carroB.setBounds(this.getWidth() - 135, 5, 30, 30);
		carroB.setVisible(tiendaViewBool);
		panel.add(carroB);

		// Componentes Filtro
		nombreProdBusquedaL = new JLabel("Nombre Producto");
		nombreProdBusquedaL.setBounds(5, 75, 120, 30);
		panel.add(nombreProdBusquedaL);

		nombreProdTF = new JTextField();
		nombreProdTF.setBounds(5, 105, 120, 30);
		panel.add(nombreProdTF);

		maxPrecioL = new JLabel("Precio maximo");
		maxPrecioL.setBounds(5, 135, 120, 30);
		panel.add(maxPrecioL);

		maxPrecioTF = new JTextField();
		maxPrecioTF.setBounds(5, 165, 120, 30);
		panel.add(maxPrecioTF);

		minPrecioL = new JLabel("Precio minimo");
		minPrecioL.setBounds(5, 190, 120, 30);
		panel.add(minPrecioL);

		minPrecioTF = new JTextField();
		minPrecioTF.setBounds(5, 215, 120, 30);
		panel.add(minPrecioTF);

		buscarFitroB = new JButton("Buscar");
		buscarFitroB.setBounds(5, 255, 120, 30);
		panel.add(buscarFitroB);
		buscarFitroB.addActionListener(this);

		// Componentes administracion
		// Productos
		adminProductosB = new JButton("Productos");
		adminProductosB.setBounds(5, 40, 120, 30);
		panel.add(adminProductosB);
		adminProductosB.addActionListener(this);

		// Usuarios
		adminUsersB = new JButton("Usuarios");
		adminUsersB.setBounds(130, 40, 120, 30);
		panel.add(adminUsersB);
		adminUsersB.addActionListener(this);

		listadoTA = new JTextArea();
		listadoTA.setEditable(false);

		// JScrollPane containerListado
		containerListado = new JScrollPane(listadoTA);
		containerListado.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		containerListado.setBounds(15, 80, 1240, 400);
		panel.add(containerListado);

		// DataFields

		for (int i = 0; i < campoTF.length; i++) {
			campoTF[i] = new JTextField();
			campoTF[i].setBounds(40 + 20 * i + 150 * i, 560, 150, 30);
			panel.add(campoTF[i]);
		}

		for (int i = 0; i < campoL.length; i++) {
			campoL[i] = new JLabel("Campo");
			campoL[i].setBounds(40 + 20 * i + 150 * i, 525, 150, 30);
			panel.add(campoL[i]);
		}

		// guardar
		guardarB = new JButton("Guardar");
		guardarB.setBounds(this.getWidth() - 200, this.getHeight() - 200, 150, 30);
		guardarB.addActionListener(this);
		panel.add(guardarB);
		// modificar
		modificarB = new JButton("Modificar");
		modificarB.setBounds(this.getWidth() - 200, this.getHeight() - 160, 150, 30);
		modificarB.addActionListener(this);
		panel.add(modificarB);
		// borrar
		borrarB = new JButton("Borrar");
		borrarB.setBounds(this.getWidth() - 200, this.getHeight() - 120, 150, 30);
		borrarB.addActionListener(this);
		panel.add(borrarB);

		// Error
		errorF = new JFrame("Error");
		errorF.setSize(300, 100);
		errorF.setResizable(false);
		errorF.setLocationRelativeTo(null);
		errorF.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		errorP = new JPanel();
		errorP.setLayout(null);

		errorL = new JLabel();
		errorL.setBounds(5, 20, 200, 30);
		errorP.add(errorL);

		errorF.add(errorP);

		// Login
		loginF = new JFrame("Login");
		loginF.setSize(300, 200);
		loginF.setResizable(false);
		loginF.setLocationRelativeTo(null);
		loginF.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		loginP = new JPanel();
		loginP.setLayout(null);

		username = new JLabel("Usuario");
		username.setBounds(50, 20, 100, 30);
		loginP.add(username);

		userTF = new JTextField();
		userTF.setBounds(100, 20, 100, 30);
		loginP.add(userTF);

		password = new JLabel("Contraseña");
		password.setBounds(30, 55, 100, 30);
		loginP.add(password);

		passPF = new JPasswordField();
		passPF.setBounds(100, 55, 100, 30);
		loginP.add(passPF);

		loginB = new JButton("Login");
		loginB.setBounds(150, 90, 100, 30);
		loginP.add(loginB);
		loginB.addActionListener(this);

		registerB = new JButton("Registrar");
		registerB.setBounds(45, 90, 100, 30);
		loginP.add(registerB);
		registerB.addActionListener(this);

		salirLogB = new JButton("Salir");
		salirLogB.setBounds(5, 125, 282, 30);
		loginP.add(salirLogB);
		salirLogB.addActionListener(this);

		loginF.add(loginP);

		actualizarVisibilidad();
		this.add(panel);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// Botones principales
		if (e.getSource() == salirB) {
			tienda.getBase().close();
			System.exit(0);
		}
		if (e.getSource() == tiendaB) {
			tiendaViewBool = true;
			administracionViewBool = false;

			actualizarVisibilidad();
		}
		if (e.getSource() == administracionB) {
			tiendaViewBool = false;
			administracionViewBool = true;

			actualizarVisibilidad();
		}
		if (e.getSource() == loginPrincB) {
			loginF.setVisible(true);
		}
		// Botones Login
		if (e.getSource() == loginB) {
			String password = "";
			for (char c : passPF.getPassword()) {
				password += c;
			}
			if (!tienda.login(userTF.getText(), password)) {
				errorL.setText("Usuario/Contraseña incorrecta.");
				errorF.setVisible(true);
			}
			try {
				nombreUsuario.setText("Usuario: " + tienda.getUser().getNombre());
				actualizarVisibilidad();
				loginF.dispose();
			} catch (NullPointerException e2) {
			}
		}
		if (e.getSource() == salirLogB) {
			loginF.dispose();
		}
		// Botones Administracion
		if (e.getSource() == adminProductosB) {
			pestana = 0;
			setDatosTabla(tienda.buscarProducto("", ""));
		}
		if (e.getSource() == adminUsersB) {
			pestana = 1;
			setDatosTabla(tienda.buscarUsuario("", ""));
		}
		if (e.getSource() == guardarB) {
			try {
				if (campoTF[0].getText().isEmpty()) {
					errorL.setText("Debe rellenar el campo del ID.");
					errorF.setVisible(true);
				} else if (campoTF[1].getText().isEmpty()) {
					errorL.setText("Debe rellenar el campo del Nombre.");
					errorF.setVisible(true);
				} else if (campoTF[2].getText().isEmpty()) {
					errorL.setText("Debe rellenar el campo del Usuario.");
					errorF.setVisible(true);
				} else if (campoTF[3].getText().isEmpty()) {
					errorL.setText("Debe rellenar el campo del Email.");
					errorF.setVisible(true);
				} else if (campoTF[4].getText().isEmpty()) {
					errorL.setText("Debe rellenar el campo de la Contraseña.");
					errorF.setVisible(true);
				} else if (campoTF[5].getText().isEmpty()) {
					errorL.setText("Debe rellenar el campo del Tipo.");
					errorF.setVisible(true);
				}
				switch (pestana) {
				case 0:
					tienda.setProductoDB(campoTF[0].getText(), campoTF[1].getText(), campoTF[2].getText(),
							campoTF[3].getText(), Double.valueOf(campoTF[4].getText()),
							Integer.valueOf(campoTF[5].getText()));
					setDatosTabla(tienda.buscarProducto("", ""));
					break;
				case 1:
					tienda.setUsuarioDB(campoTF[0].getText(), campoTF[1].getText(), campoTF[2].getText(),
							campoTF[3].getText(), campoTF[4].getText(), Integer.valueOf(campoTF[5].getText()));
					setDatosTabla(tienda.buscarUsuario("", ""));
					break;
				}
			} catch (NumberFormatException e2) {

			}
		}
		if (e.getSource() == modificarB) {
			if (campoTF[0].getText().isEmpty()) {
				errorL.setText("Debe rellenar el campo del ID.");
				errorF.setVisible(true);
			} else {
				switch (pestana) {
				case 0:
					try {
						tienda.setProductoDB(campoTF[0].getText(), campoTF[1].getText(), campoTF[2].getText(),
								campoTF[3].getText(), Double.valueOf(campoTF[4].getText()),
								Integer.valueOf(campoTF[5].getText()));
					} catch (NumberFormatException e2) {
						tienda.setProductoDB(campoTF[0].getText(), campoTF[1].getText(), campoTF[2].getText(),
								campoTF[3].getText(), 0, 0);
					}
					setDatosTabla(tienda.buscarProducto("", ""));
					break;
				case 1:
					tienda.setUsuarioDB(campoTF[0].getText(), campoTF[1].getText(), campoTF[2].getText(),
							campoTF[3].getText(), campoTF[4].getText(), Integer.valueOf(campoTF[5].getText()));
					setDatosTabla(tienda.buscarUsuario("", ""));
					break;
				}
			}

		}
		if (e.getSource() == borrarB) {
			if (campoTF[0].getText().isEmpty()) {
				errorL.setText("Debe rellenar el campo del ID.");
				errorF.setVisible(true);
			} else {
				switch (pestana) {
				case 0:
					tienda.eliminarProducto(tienda.buscarProducto(campoTF[0].getText(), campoTF[1].getText()));
					setDatosTabla(tienda.buscarProducto("", ""));
					break;
				case 1:
					tienda.eliminarUsuarioDB(tienda.buscarUsuario(campoTF[0].getText(), campoTF[3].getText()));
					setDatosTabla(tienda.buscarUsuario("", ""));
					break;
				}
			}

		}

	}

	public void actualizarVisibilidad() {
		// Visibilidad Tienda
		carroB.setVisible(tiendaViewBool);
		nombreProdBusquedaL.setVisible(tiendaViewBool);
		nombreProdTF.setVisible(tiendaViewBool);
		maxPrecioL.setVisible(tiendaViewBool);
		maxPrecioTF.setVisible(tiendaViewBool);
		minPrecioL.setVisible(tiendaViewBool);
		minPrecioTF.setVisible(tiendaViewBool);
		buscarFitroB.setVisible(tiendaViewBool);

		// visibilidad Administracion
		adminProductosB.setVisible(administracionViewBool);
		containerListado.setVisible(administracionViewBool);
		for (int i = 0; i < campoL.length; i++) {
			campoL[i].setVisible(administracionViewBool);
			campoTF[i].setVisible(administracionViewBool);
		}
		guardarB.setVisible(administracionViewBool);
		modificarB.setVisible(administracionViewBool);
		borrarB.setVisible(administracionViewBool);

		if (tienda.getUser() == null || tienda.getUser().getTipo() > 0) {
			adminUsersB.setVisible(administracionViewBool && false);
		} else {
			adminUsersB.setVisible(administracionViewBool);
		}

		if (tienda.getUser() == null || tienda.getUser().getTipo() >= 1) {
			administracionB.setVisible(false);

		} else {
			administracionB.setVisible(true);
		}
	}

	public void cargarProductos() {

	}

	public void setDatosTabla(ObjectSet<?> dataObject) {
		switch (pestana) {
		case 0:
			cabecera = "Id Producto\tNombre\t\tDescripcion\t\tPrecio\tCantidad\n";
			campoL[0].setText("Id Producto");
			campoL[2].setText("Descripcion");
			campoL[3].setText("Ruta Imagen");
			campoL[4].setText("Precio");
			campoL[5].setText("Cantidad");
			break;
		case 1:
			cabecera = "Id Usuario\tNombre\tUsername\tEmail\t\tTipo\n";
			campoL[0].setText("Id Usuario");
			campoL[2].setText("Username");
			campoL[3].setText("Email");
			campoL[4].setText("Contraseña");
			campoL[5].setText("Tipo");
			break;
		}
		campoL[1].setText("Nombre");

		data = tienda.listar(dataObject);
		listadoTA.setText(cabecera + data);
	}
}