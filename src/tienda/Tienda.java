package tienda;

import java.util.ArrayList;
import java.util.Iterator;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;

import tienda.productos.Producto;
import tienda.usuarios.Carro;
import tienda.usuarios.Recibo;
import tienda.usuarios.Usuario;

public class Tienda {
	private ObjectContainer base;
	private Usuario user;
	private Carro carro;

	public Tienda(ObjectContainer base) {
		this.base = base;
	}

	// Funciones
	
	public Usuario getUser() {
		return user;
	}

	public ObjectContainer getBase() {
		return base;
	}
	
	// Productos
	// Busca un producto en la base de datos
	// Busqueda por objeto
	public ObjectSet<Producto> buscarProducto(Producto producto) {
		ObjectSet<Producto> resultado = base.queryByExample(producto);
		return resultado;
	}

	// Busqueda por id o nombre
	public ObjectSet<Producto> buscarProducto(String idProducto, String nombre) {
		Producto producto = new Producto(null, null, null, null, 0, 0);
		if (!idProducto.isEmpty()) {
			producto.setIdProducto(idProducto);
		}
		if (!nombre.isEmpty()) {
			producto.setNombre(nombre);
		}

		ObjectSet<Producto> resultado = base.queryByExample(producto);
		return resultado;
	}

	// Busqueda por rango de precio
	public ObjectSet<Producto> buscarProducto(double precioMax, double precioMin) {
		@SuppressWarnings("serial")
		Predicate<Producto> consulta = new Predicate<Producto>() {
			@Override
			public boolean match(Producto prod) {
				return (prod.getPrecio() >= precioMin && prod.getPrecio() <= precioMax);
			}
		};

		ObjectSet<Producto> resultado = base.query(consulta);
		return resultado;
	}

	// Añade/modifica un porducto en la base de datos
	public void setProductoDB(String idProducto, String nombre, String descripcion, String imagen, double precio,
			int cantidad) {
		Producto producto;
		ObjectSet<Producto> resultado = buscarProducto(idProducto, "");

		if (resultado.isEmpty()) { // si no se encuentra
			producto = new Producto(idProducto, nombre, descripcion, imagen, precio, cantidad);
		} else { // si se encuentra se modifica
			producto = (Producto) resultado.get(0);
			if (!nombre.isEmpty()) {
				producto.setNombre(nombre);
			}
			if (!descripcion.isEmpty()) {
				producto.setDescripcion(descripcion);
			}
			if (precio > 0) {
				producto.setPrecio(precio);
			}
			if (cantidad > 0) {
				producto.setCantidad(cantidad);
			}
		}
		base.store(producto);
	}

	public void restarCantidadDB(Producto productoNew) {
		// Busca si el producto ya pertenece a la base de datos
		Producto producto;
		@SuppressWarnings("serial")
		Predicate<Producto> consulta = new Predicate<Producto>() {
			@Override
			public boolean match(Producto prod) {
				return (prod.getIdProducto() == productoNew.getIdProducto());
			}
		};

		ObjectSet<Producto> resultado = base.query(consulta);
		producto = productoNew;
		producto = resultado.get(0);
		producto.restarCantidad(productoNew.getCantidad());

		base.store(producto);

	}

	// Elimina un producto de la base de datos
	public boolean eliminarProducto(ObjectSet<Producto> objectSet) {
		if (objectSet.size() > 0) {
			Producto producto = (Producto) objectSet.get(0);
			base.delete(producto);
			return true;
		}
		return false;
	}
	// Carro

	// Almacenar carro en base de datos
	public void guardarCarro() {
		if (this.carro == null) {
			buscarCarro();
		}
		actualizarCarro();
		base.store(this.carro);
	}

	// Buscar carro en base de datos
	public void buscarCarro() {
		Carro carro = new Carro(user.getIdUsuario());
		ObjectSet<Carro> resultado = base.queryByExample(carro);
		if (resultado.size() == 1) {
			this.carro = resultado.get(0);
		} else {
			this.carro = new Carro(user.getIdUsuario());
		}
	}

	// Actualiza la informacion del carro con la base de datos
	public void actualizarCarro() {
		ArrayList<Producto> productos = carro.getProductos();
		Iterator<Producto> carroIt = productos.iterator();
		while (carroIt.hasNext()) {
			Producto p = carroIt.next();
			Producto pAct = buscarProducto(p.getIdProducto(), "").get(0);
			productos.set(productos.indexOf(p), pAct);
		}
		carro.setProductos(productos);
	}

	// Compra productos
	public void comprarProducto() {
		if (user != null) {
			Carro recibo = new Recibo(user.getIdUsuario());
			buscarCarro();
			actualizarCarro();
			recibo = carro;
			carro = null;
			Iterator<Producto> productosCarroIt = recibo.getProductos().iterator();
			while (productosCarroIt.hasNext()) {
				Producto productoCarro = productosCarroIt.next();
				restarCantidadDB(productoCarro);
			}
		}
	}

	// Genera String con la lista de la base de datos
	public String listar(ObjectSet<?> objectSet) {
		String consulta = "";
		while (objectSet.hasNext()) {
			consulta += objectSet.next() + "\n";
		}
		return consulta;
	}

	
	// Usuario
	// Comprueba los datos del usuario antes de usarlos como usuario
	public boolean login(String usuario, String pass) {
		Usuario userL = new Usuario(null, null, usuario, null, null, 0);
		ObjectSet<Usuario> resultado = base.queryByExample(userL);
		if (resultado.size() == 1) {
			if (resultado.get(0).getPass().contentEquals(pass)) {
				this.user = resultado.get(0);
				return true;
			}
		}
		return false;
	}

	// Añade/Modifica un usuario a la base de datos
	public void setUsuarioDB(String idUsuario, String nombre, String username, String email, String pass, int tipo) {
		Usuario userR;

		ObjectSet<Usuario> resultado = buscarUsuario(idUsuario, "");

		if (resultado.isEmpty()) { // si no se encuentra
			userR = new Usuario(idUsuario, nombre, username, email, pass, tipo);
		} else { // si se encuentra se modifica
			userR = (Usuario) resultado.get(0);
			if (!nombre.isEmpty()) {
				userR.setNombre(nombre);
			}
			if (!username.isEmpty()) {
				userR.setUsuario(username);
			}
			if (!email.isEmpty()) {
				userR.setEmail(email);
			}
			if (!pass.isEmpty()) {
				userR.setPass(pass);
			}
			if (tipo > 0) {
				userR.setTipo(tipo);
			}
		}

		base.store(userR);
	}

	public boolean eliminarUsuarioDB(ObjectSet<Usuario> objectSet) {
		if (objectSet.size() > 0) {
			while (objectSet.hasNext()) {
				Usuario usuario = objectSet.next();
				base.delete(usuario);
			}
			return true;
		}
		return false;
	}

	public ObjectSet<Usuario> buscarUsuario(String idUsuario, String email) {
		Usuario usuario = new Usuario(null, null, null, null, null, 0);
		if (!idUsuario.isEmpty()) {
			usuario.setIdUsuario(idUsuario);
		}
		if (!email.isEmpty()) {
			usuario.setEmail(email);
		}

		ObjectSet<Usuario> resultado = base.queryByExample(usuario);
		return resultado;
	}

}
