package tienda.usuarios;

public class Usuario {
	private String idUsuario;
	private String nombre;
	private String username;
	private String email;
	private String pass;
	private int tipo; // 0 administrador, 1 vendedor, 2 cliente

	public Usuario(String idUsuario, String nombre, String username, String email, String pass, int tipo) {
		this.idUsuario = idUsuario;
		this.nombre = nombre;
		this.username = username;
		this.email = email;
		this.pass = pass;
		this.tipo = tipo;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public String getNombre() {
		return nombre;
	}

	public String getUsuario() {
		return username;
	}

	public String getEmail() {
		return email;
	}

	public String getPass() {
		return pass;
	}

	public int getTipo() {
		return tipo;
	}

	public String getTipoT() {
		switch (tipo) {
		case 0:
			return "Adminsitrador";
		case 1:
			return "Vendedor";
		}
		return "Cliente";
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setUsuario(String usuario) {
		this.username = usuario;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return idUsuario + "\t" + nombre + "\t" + username + "\t" + email
				+ "\t" + tipo + " - " + getTipoT();
	}

	
}
