package tienda.usuarios;

import java.time.LocalDateTime;

public final class Recibo extends Carro{
	private LocalDateTime fecha;

	public Recibo(String idUsuario) {
		super(idUsuario);
		this.fecha = LocalDateTime.now();
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}
	
	
	

}
