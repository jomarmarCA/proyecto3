package tienda.usuarios;

import java.util.ArrayList;
import java.util.Iterator;

import tienda.productos.Producto;

public class Carro {
	private String idUsuario;
	private double total;
	private ArrayList<Producto> productos;

	public Carro(String idUsuario) {
		this.idUsuario = idUsuario;
		total = 0;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public ArrayList<Producto> getProductos() {
		return productos;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal() {
		Iterator<Producto> productosCarroIt = productos.iterator();
		while (productosCarroIt.hasNext()) {
			Producto productoCarro = productosCarroIt.next();
			total += productoCarro.getPrecio();
		}
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public void setProductos(ArrayList<Producto> productos) {
		this.productos = productos;
	}

	public void vaciarCarro() {
		productos.clear();
	}

	public void anadeProducto(Producto producto) {
		productos.add(producto);
	}

	public void quitarProducto(String idProducto) {
		Iterator<Producto> prodIt = productos.iterator();
		while (prodIt.hasNext()) {
			Producto prod = prodIt.next();
			if (prod.getIdProducto().contentEquals(idProducto)) {
				productos.remove(prod);
			}
		}
	}

	public void cambiaCantidad(String idProducto, int cantidad) {
		int index;
		Iterator<Producto> prodIt = productos.iterator();
		while (prodIt.hasNext()) {
			Producto prod = prodIt.next();
			if (prod.getIdProducto().contentEquals(idProducto)) {
				index = productos.indexOf(prod);
				prod.setCantidad(cantidad);
				productos.set(index, prod);
			}
		}
	}

	@Override
	public String toString() {
		return "Carro [idUsuario=" + idUsuario + ", productos=" + productos + "]";
	}
}
