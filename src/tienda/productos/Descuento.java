package tienda.productos;

import java.util.ArrayList;

public class Descuento {
	private String nombre;
	private double porcentaje;
	private ArrayList<String> idProducto;
	private ArrayList<String> idUsuario;
	
	public Descuento(String nombre, double porcentaje, ArrayList<String> idProducto, ArrayList<String> idUsuario) {
		this.nombre = nombre;
		this.porcentaje = porcentaje;
		this.idProducto = idProducto;
		this.idUsuario = idUsuario;
	}

	public String getNombre() {
		return nombre;
	}

	public double getPorcentaje() {
		return porcentaje;
	}

	public ArrayList<String> getIdProducto() {
		return idProducto;
	}

	public ArrayList<String> getIdUsuario() {
		return idUsuario;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setPorcentaje(double porcentaje) {
		this.porcentaje = porcentaje;
	}

	public void setIdProducto(ArrayList<String> idProducto) {
		this.idProducto = idProducto;
	}

	public void setIdUsuario(ArrayList<String> idUsuario) {
		this.idUsuario = idUsuario;
	}

	@Override
	public String toString() {
		return "Descuento [nombre=" + nombre + ", porcentaje=" + porcentaje + ", idProducto=" + idProducto
				+ ", idUsuario=" + idUsuario + "]";
	}
	
	

}
