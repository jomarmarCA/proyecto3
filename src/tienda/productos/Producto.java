package tienda.productos;

public class Producto {
	private String idProducto;
	private String nombre;
	private String descripcion;
	private String imagen;
	private double precio;
	private int cantidad;

	public Producto(String idProducto, String nombre, String descripcion, String imagen, double precio, int cantidad) {
		this.idProducto = idProducto;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.imagen = imagen;
		this.precio = precio;
		this.cantidad = cantidad;
	}

	public String getIdProducto() {
		return idProducto;
	}

	public String getNombre() {
		return nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getImagen() {
		return imagen;
	}

	public double getPrecio() {
		return precio;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setIdProducto(String idProducto) {
		this.idProducto = idProducto;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public void restarCantidad(int cantidad) {
		this.cantidad -= cantidad;
	}

	public void anadirCantidad(int cantidad) {
		this.cantidad += cantidad;
	}

	@Override
	public String toString() {
		return idProducto + "\t" + nombre + "\t" + descripcion
				+ "\t" + precio + "\t" + cantidad;
	}

}
