package tienda.productos;

public final class Ropa extends Producto {
	private String talla;
	private String color;
	private String material;

	public Ropa(String idProducto, String nombre, String descripcion, String imagen, double precio, int cantidad,
			String talla, String color, String material) {
		super(idProducto, nombre, descripcion, imagen, precio, cantidad);
		this.talla = talla;
		this.color = color;
		this.material = material;
	}

	public String getTalla() {
		return talla;
	}

	public String getColor() {
		return color;
	}

	public String getMaterial() {
		return material;
	}

	public void setTalla(String talla) {
		this.talla = talla;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

}
